﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace DynamicGeometry
{
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();
            toolBar1.ItemsSource = Behavior.Singletons;
            this.Loaded += Window1_Loaded;
        }

        void Window1_Loaded(object sender, RoutedEventArgs e)
        {
            CurrentDrawing = new Drawing(canvas1);
        }

        Drawing mCurrentDrawing;
        Drawing CurrentDrawing
        {
            get
            {
                return mCurrentDrawing;
            }
            set
            {
                if (mCurrentDrawing != null)
                {
                    mCurrentDrawing.Canvas = null;
                }
                mCurrentDrawing = value;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Behavior b = (sender as Button).Tag as Behavior;
            if (b != null)
            {
                CurrentDrawing.Behavior = b;
            }
        }

        private void Undo_Click(object sender, RoutedEventArgs e)
        {
            CurrentDrawing.ActionManager.Undo();
        }

        private void Redo_Click(object sender, RoutedEventArgs e)
        {
            CurrentDrawing.ActionManager.Redo();
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Close();
        }

        public void Load(string path)
        {
            if (path != null && File.Exists(path))
            {
                HandleExceptions(() =>
                    CurrentDrawing = Drawing.Load(path, canvas1)
                );
            }
        }

        public void Save(string path)
        {
            if (path != null)
            {
                HandleExceptions(() =>
                    CurrentDrawing.Save(path)
                );
            }
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            HandleExceptions(() =>
                CurrentDrawing = new Drawing(canvas1)
            );
        }

        public void HandleExceptions(Action code)
        {
            try
            {
                code();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Open_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog();
            openFileDialog.Filter = "Dynamic Geometry files (*.DynG)|*.DynG|All files (*.*)|*.*";

            if (openFileDialog.ShowDialog().Value)
            {
                this.Load(openFileDialog.FileName);
            }
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog saveFileDialog = new Microsoft.Win32.SaveFileDialog();
            saveFileDialog.Filter = "Dynamic Geometry files (*.DynG)|*.DynG|All files (*.*)|*.*";
            saveFileDialog.AddExtension = true;

            if (saveFileDialog.ShowDialog().Value)
            {
                this.Save(saveFileDialog.FileName);
            }
        }

        private void Cut_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Copy_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Paste_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            DeleteSelection();
        }

        private void DeleteSelection()
        {
            HandleExceptions(() =>
                CurrentDrawing.Delete()
            );
        }

        private void SelectAll_Click(object sender, RoutedEventArgs e)
        {
            SelectAll();
        }

        private void SelectAll()
        {
            HandleExceptions(() =>
                CurrentDrawing.SelectAll()
            );
        }

        protected override void OnKeyUp(System.Windows.Input.KeyEventArgs e)
        {
            base.OnKeyUp(e);

            switch (e.Key)
            {
                case System.Windows.Input.Key.Z:
                    if (e.KeyboardDevice.IsKeyDown(System.Windows.Input.Key.RightCtrl) || e.KeyboardDevice.IsKeyDown(System.Windows.Input.Key.LeftCtrl))
                        CurrentDrawing.ActionManager.Undo();
                    break;
                case System.Windows.Input.Key.Y:
                    if (e.KeyboardDevice.IsKeyDown(System.Windows.Input.Key.RightCtrl) || e.KeyboardDevice.IsKeyDown(System.Windows.Input.Key.LeftCtrl))
                        CurrentDrawing.ActionManager.Redo();
                    break;
                case System.Windows.Input.Key.A:
                    if (e.KeyboardDevice.IsKeyDown(System.Windows.Input.Key.RightCtrl) || e.KeyboardDevice.IsKeyDown(System.Windows.Input.Key.LeftCtrl))
                        SelectAll();
                    break;
                case System.Windows.Input.Key.Delete:
                    DeleteSelection();
                    break;
            }
        }
    }
}
