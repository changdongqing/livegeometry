﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace DynamicGeometry
{
    public class AxisLabel
    {
        public AxisLabel()
        {
            TextBlock = new TextBlock();
            TextBlock.Foreground = CartesianGrid.AxesBrush;
        }

        public TextBlock TextBlock { get; set; }

        public void PositionOnXAxis(double x, CoordinateSystem coordinateSystem)
        {
            SetLabelText(TextBlock, x);
            var coordinates = coordinateSystem
                .ToPhysical(new Point(x, 0))
                .OffsetX(-TextBlock.ActualWidth / 2);
            if (x == 0)
            {
                coordinates = coordinates.OffsetX(-TextBlock.ActualWidth / 2 - 2);
            }
            MoveLabel(TextBlock, coordinates);
        }

        public void PositionOnYAxis(double y, CoordinateSystem coordinateSystem)
        {
            SetLabelText(TextBlock, y);
            var coordinates = coordinateSystem
                .ToPhysical(new Point(0, y))
                .Plus(new Point(-TextBlock.ActualWidth - 2, -TextBlock.ActualHeight / 2));
            MoveLabel(TextBlock, coordinates);
        }

        private void MoveLabel(TextBlock label, Point coordinates)
        {
            Point old = TextBlock.GetCoordinates();
            if (old != coordinates)
            {
                TextBlock.MoveTo(coordinates);
            }
        }

        void SetLabelText(TextBlock label, double x)
        {
            string text = x.ToString(CultureInfo.InvariantCulture);
            if (label.Text != text)
            {
                label.Text = text;
            }
        }
    }

    public class AxisLabelRange
    {
        public AxisLabelRange(AxisLabelsCollection collection)
        {
            Collection = collection;
        }

        public AxisLabelsCollection Collection { get; set; }

        public Drawing Drawing
        {
            get
            {
                return Collection.Drawing;
            }
        }
        public CoordinateSystem CoordinateSystem
        {
            get
            {
                return Drawing.CoordinateSystem;
            }
        }

        public void OnAddingToCanvas(Canvas canvas)
        {
            List.ForEach(t => canvas.Children.Add(t.TextBlock));
        }

        public void OnRemovingFromCanvas(Canvas canvas)
        {
            List.ForEach(t => canvas.Children.Remove(t.TextBlock));
        }

        private bool mVisible = true;
        public bool Visible
        {
            get
            {
                return mVisible;
            }
            set
            {
                mVisible = value;
                List.ForEach(t => t.TextBlock.Visibility = value ? Visibility.Visible : Visibility.Collapsed);
            }
        }

        List<AxisLabel> List = new List<AxisLabel>();

        public void AdjustToNewRange(IEnumerable<double> newPoints, bool isX)
        {
            int count = newPoints.Count();

            if (List.Count < count)
            {
                AddMissingElements(List, count - List.Count);
            }
            else if (List.Count > count)
            {
                RemoveExcessElements(List, List.Count - count);
            }

            int current = 0;
            foreach (var coordinate in newPoints)
            {
                if (isX)
                {
                    List[current++].PositionOnXAxis(coordinate, CoordinateSystem);
                }
                else
                {
                    List[current++].PositionOnYAxis(coordinate, CoordinateSystem);
                }
            }
        }

        void RemoveExcessElements(List<AxisLabel> List, int count)
        {
            for (int i = List.Count - count; i < List.Count; i++)
            {
                Drawing.Canvas.Children.Remove(List[i].TextBlock);
            }
            List.RemoveRange(List.Count - count, count);
        }

        void AddMissingElements(List<AxisLabel> List, int count)
        {
            for (int i = 0; i < count; i++)
            {
                var newLabel = new AxisLabel();
                List.Add(newLabel);
                newLabel.TextBlock.Visibility = this.Visible.ToVisibility();
                Drawing.Canvas.Children.Add(newLabel.TextBlock);
            }
        }
    }

    public class AxisLabelsCollection : FigureBase
    {
        public AxisLabelsCollection()
        {
            XAxisLabels = new AxisLabelRange(this);
            YAxisLabels = new AxisLabelRange(this);
        }

        AxisLabelRange XAxisLabels { get; set; }
        AxisLabelRange YAxisLabels { get; set; }

        public override void UpdateVisual()
        {
            XAxisLabels.AdjustToNewRange(Drawing.CoordinateSystem.GetVisibleXPoints(), true);
            YAxisLabels.AdjustToNewRange(Drawing.CoordinateSystem.GetVisibleYPoints()
                .Where(y => y.Abs() > 0.001), false);
        }

        public override bool Visible
        {
            get
            {
                return base.Visible;
            }
            set
            {
                base.Visible = value;
                if (XAxisLabels != null)
                {
                    XAxisLabels.Visible = value;
                }
                if (YAxisLabels != null)
                {
                    YAxisLabels.Visible = value;
                }
            }
        }

        public override IFigure HitTest(Point point)
        {
            return null;
        }

        public override void OnAddingToCanvas(Canvas newContainer)
        {
            XAxisLabels.OnAddingToCanvas(newContainer);
            YAxisLabels.OnAddingToCanvas(newContainer);
        }

        public override void OnRemovingFromCanvas(Canvas leavingContainer)
        {
            XAxisLabels.OnRemovingFromCanvas(leavingContainer);
            YAxisLabels.OnRemovingFromCanvas(leavingContainer);
        }
    }
}
