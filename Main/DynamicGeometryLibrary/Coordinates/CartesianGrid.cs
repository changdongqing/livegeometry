﻿using System.Windows.Media;
///5/1a/spx//
namespace DynamicGeometry
{
    public class CartesianGrid : CompositeFigure
    {
        public CartesianGrid()
        {
            OriginPoint = Factory.CreatePointByCoordinates(Drawing, () => 0, () => 0);
            XUnitPoint = Factory.CreatePointByCoordinates(Drawing, () => 1, () => 0);
            YUnitPoint = Factory.CreatePointByCoordinates(Drawing, () => 0, () => 1);
            OriginPoint.Visible = false;
            XUnitPoint.Visible = false;
            YUnitPoint.Visible = false;
            XAxisLine = Factory.CreateAxis(Drawing, new FigureList() { OriginPoint, XUnitPoint });
            YAxisLine = Factory.CreateAxis(Drawing, new FigureList() { OriginPoint, YUnitPoint });

            AxisLabels = new AxisLabelsCollection() { Drawing = Drawing };
            GridLines = new RectangularGridLinesCollection() { Drawing = Drawing };

            Add(
                OriginPoint,
                XUnitPoint,
                YUnitPoint,
                XAxisLine,
                YAxisLine,
                AxisLabels,
                GridLines);
        }

        public PointByCoordinates OriginPoint { get; set; }
        public PointByCoordinates XUnitPoint { get; set; }
        public PointByCoordinates YUnitPoint { get; set; }
        public Axis XAxisLine { get; set; }
        public Axis YAxisLine { get; set; }
        AxisLabelsCollection AxisLabels { get; set; }
        GridLinesCollection GridLines { get; set; }

        public override bool Visible
        {
            get
            {
                return base.Visible;
            }
            set
            {
                base.Visible = value;
                AxisLabels.Visible = value;
                GridLines.Visible = value;
                XAxisLine.Visible = value;
                YAxisLine.Visible = value;
            }
        }

        public static Brush AxesBrush = new SolidColorBrush(Axis.Color);
    }
}
