﻿using System.Collections.Generic;
using System.Xml.Linq;

namespace DynamicGeometry
{
    public class DrawingStorage
    {
        public DrawingStorage(string content)
        {
            Storage = XElement.Parse(content);
        }

        private XElement Storage { get; set; }

        public IEnumerable<XElement> Drawings
        {
            get
            {
                return Storage.Elements("Drawing");
            }
        }

        public void Add(XElement drawing)
        {
            Storage.Add(drawing);
        }

        public override string ToString()
        {
            return Storage.ToString();
        }
    }
}
