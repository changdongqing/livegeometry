using System.Collections.Generic;

namespace GuiLabs.Utils.Actions
{
	public interface IMultiAction : IAction, IList<IAction>
	{
        bool IsDelayed { get; set; }
	}

    public class MultiAction : List<IAction>, IMultiAction
    {
        public MultiAction()
        {
            IsDelayed = true;
        }

        public bool IsDelayed { get; set; }

        public void Execute()
        {
            if (!IsDelayed)
            {
                IsDelayed = true;
                return;
            }
            foreach (var action in this)
            {
                action.Execute();
            }
        }

        public void UnExecute()
        {
            foreach (var action in this)
            {
                action.UnExecute();
            }
        }

        public bool CanExecute()
        {
            foreach (var action in this)
            {
                if (!action.CanExecute())
                {
                    return false;
                }
            }
            return true;
        }

        public bool CanUnExecute()
        {
            foreach (var action in this)
            {
                if (!action.CanUnExecute())
                {
                    return false;
                }
            }
            return true;
        }

        public bool TryToMerge(IAction FollowingAction)
        {
            return false;
        }

        private bool mAllowToMergeWithPrevious = false;
        public bool AllowToMergeWithPrevious
        {
            get
            {
                return mAllowToMergeWithPrevious;
            }
            set
            {
                mAllowToMergeWithPrevious = value;
            }
        }
    }
}
