namespace GuiLabs.Utils.Actions
{
    public class TransactionBase : ITransaction
    {
        #region ctors

        public TransactionBase(ActionManager am, bool isDelayed) : this(am)
        {
            IsDelayed = isDelayed;
        }

        public TransactionBase(ActionManager am)
            : this()
        {
            ActionManager = am;
            if (am != null)
            {
                am.OpenTransaction(this);
            }
        }

        public TransactionBase()
        {
            IsDelayed = true;
        }

        public bool IsDelayed { get; set; }

        #endregion

        #region MultiAction

        protected IMultiAction mAccumulatingAction;
        public IMultiAction AccumulatingAction
        {
            get
            {
                return mAccumulatingAction;
            }
        }

        #endregion

        #region Commit

        public virtual void Commit()
        {
            if (ActionManager != null)
            {
                ActionManager.CommitTransaction();
            }
        }

        #endregion

        #region Rollback

        public virtual void Rollback()
        {
            if (ActionManager != null)
            {
                ActionManager.RollBackTransaction();
            }
        }

        #endregion

        #region ActionManager

        private ActionManager mActionManager;
        public ActionManager ActionManager
        {
            get
            {
                return mActionManager;
            }
            private set
            {
                if (value == null)
                {
                    //throw new InvalidOperationException(
                    //    "Transaction.Root should never be null.");
                }

                mActionManager = value;
                //if (mActionManager != null)
                //{
                //    mAccumulatingAction = new MultiAction(mRoot);
                //}
            }
        }

        #endregion

        #region Dispose

        public void Dispose()
        {
            Commit();
        }

        #endregion
    }
}
