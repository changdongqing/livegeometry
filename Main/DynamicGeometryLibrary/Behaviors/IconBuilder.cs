﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.Windows.Markup;

namespace DynamicGeometry
{
    public class IconBuilder
    {
        public IconBuilder() : this(Behavior.IconSize)
        {
        }

        public IconBuilder(double size)
        {
            Canvas = new Canvas();
            Canvas.Width = size;
            Canvas.Height = Canvas.Width;
        }

        public Canvas Canvas { get; set; }

        public static IconBuilder BuildIcon()
        {
            return new IconBuilder();
        }

        public static IconBuilder BuildIcon(double size)
        {
            return new IconBuilder(size);
        }

        public IconBuilder Point(double x, double y)
        {
            Shape point = Factory.CreatePointShape();
            Canvas.Children.Add(point);
            Canvas.SetLeft(point, Canvas.Width * x - point.Width / 2);
            Canvas.SetTop(point, Canvas.Height * y - point.Height / 2);
            return this;
        }

        public IconBuilder DependentPoint(double x, double y)
        {
            Shape point = Factory.CreateDependentPointShape();
            Canvas.Children.Add(point);
            Canvas.SetLeft(point, Canvas.Width * x - point.Width / 2);
            Canvas.SetTop(point, Canvas.Height * y - point.Height / 2);
            return this;
        }

        public IconBuilder Line(double x1, double y1, double x2, double y2)
        {
            Line line = Factory.CreateLineShape();
            Canvas.Children.Add(line);
            line.X1 = Canvas.Width * x1;
            line.Y1 = Canvas.Height * y1;
            line.X2 = Canvas.Width * x2;
            line.Y2 = Canvas.Height * y2;
            return this;
        }

        public IconBuilder Circle(double x, double y, double radius)
        {
            Shape circle = Factory.CreateCircleShape();
            Canvas.Children.Add(circle);
            circle.Width = Canvas.Width * radius * 2;
            circle.Height = Canvas.Height * radius * 2;
            Canvas.SetLeft(circle, Canvas.Width * x - circle.Width / 2);
            Canvas.SetTop(circle, Canvas.Height * y - circle.Height / 2);
            return this;
        }

        public IconBuilder Arc(double xc, double yc, double x1, double y1, double x2, double y2)
        {
            var arcInfo = Factory.CreateArcShape();
            arcInfo.Item2.StartPoint = new Point(Canvas.Width * x1, Canvas.Height * y1);
            arcInfo.Item3.Point = new Point(Canvas.Width * x2, Canvas.Height * y2);
            var radius = Math.Distance(xc, yc, x1, y1);
            arcInfo.Item3.Size = new Size(Canvas.Width * radius, Canvas.Height * radius);
            arcInfo.Item3.SweepDirection = System.Windows.Media.SweepDirection.Counterclockwise;
            arcInfo.Item3.IsLargeArc = false;
            Canvas.Children.Add(arcInfo.Item1);
            return this;
        }

        public System.Windows.Shapes.Polygon AddPolygon(IEnumerable<Point> points)
        {
            var polygon = Factory.CreatePolygonShape();
            Canvas.Children.Add(polygon);
            foreach (var p in points)
            {
                polygon.Points.Add(new Point(
                        Canvas.Width * p.X,
                        Canvas.Height * p.Y));
            }
            return polygon;
        }

        public System.Windows.Shapes.Polygon AddPolygon(params System.Windows.Point[] points)
        {
            return AddPolygon((IEnumerable<Point>)points);
        }

        public IconBuilder Polygon(IEnumerable<Point> points)
        {
            AddPolygon(points);
            return this;
        }

        public IconBuilder Polygon(params System.Windows.Point[] points)
        {
            return Polygon((IEnumerable<Point>)points);
        }
    }
}
