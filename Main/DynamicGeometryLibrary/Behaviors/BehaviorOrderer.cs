﻿using System;
using System.Collections.Generic;

namespace DynamicGeometry
{
    public class BehaviorOrderer
    {
        public static void Order(List<Behavior> behaviors)
        {
            behaviors.Sort((x, y) => GetOrder(x).CompareTo(GetOrder(y)));
        }

        private static Dictionary<Type, int> orders = new Dictionary<Type, int>()
        {
            {typeof(Dragger), 1},
            {typeof(FreePointCreator), 10},
            {typeof(SegmentCreator), 20},
            {typeof(RayCreator), 30},
            {typeof(LineTwoPointsCreator), 40},
            {typeof(ParallelLineCreator), 50},
            {typeof(PerpendicularLineCreator), 60},
            {typeof(CircleCreator), 70},
            {typeof(CircleByRadiusCreator), 80},
            {typeof(ArcCreator), 85},
            {typeof(MidpointCreator), 90},
            {typeof(SymmPointCreator), 100},
            {typeof(PolygonCreator), 110},
            {typeof(LocusCreator), 120},
            {typeof(CoordinatesMeasurementCreator), 190},
            {typeof(DistanceMeasurementCreator), 200},
            {typeof(AngleMeasurementCreator), 210},
            {typeof(AreaMeasurementCreator), 220}
        };

        public static int GetOrder(Behavior behavior)
        {
            int result = 1000;
            if (orders.TryGetValue(behavior.GetType(), out result))
            {
                return result;
            }
            return 1000;
        }
    }
}
