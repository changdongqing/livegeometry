﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;

namespace DynamicGeometry
{
    public interface IFigures : IEnumerable<IFigure>
    {
        IFigure this[int index] { get; set; }
    }

    public interface IFigureList :
        IFigure,
        IList<IFigure>,
        INotifyCollectionChanged,
        INotifyPropertyChanged
    {
        IFigureList Clone();
    }
}