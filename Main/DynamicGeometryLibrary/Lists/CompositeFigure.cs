﻿namespace DynamicGeometry
{
    public class CompositeFigure : FigureList
    {
        public CompositeFigure()
        {
            Name = this.GetType().Name + FigureBase.ID;
        }

        protected override void OnItemAdded(IFigure item)
        {
            item.RegisterWithDependencies();
        }

        protected override void OnItemRemoved(IFigure item)
        {
            item.UnregisterFromDependencies();
        }
    }
}
//源码下载及讨论地址：http://www.51aspx.com/CV/SLGeometry