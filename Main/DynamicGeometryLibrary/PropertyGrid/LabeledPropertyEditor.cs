﻿using System.Reflection;
using System.Windows;
using System.Windows.Controls;

namespace DynamicGeometry
{
    public abstract class LabeledPropertyEditor : Grid
    {
        public LabeledPropertyEditor()
        {
            this.ColumnDefinitions.Add(new ColumnDefinition()
            {
                Width = GridLength.Auto,
                MinWidth = 60
            });
            this.ColumnDefinitions.Add(new ColumnDefinition());
            this.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            Label = new TextBlock();
            Label.VerticalAlignment = VerticalAlignment.Center;
            Label.Margin = new System.Windows.Thickness(4, 4, 8, 4);
            Label.SetValue(Grid.ColumnProperty, 0);
            var editor = CreateEditor();
            editor.SetValue(Grid.ColumnProperty, 1);
            this.Children.Add(Label);
            this.Children.Add(editor);
            this.Loaded += new RoutedEventHandler(LabeledPropertyEditor_Loaded);
        }

        void LabeledPropertyEditor_Loaded(object sender, RoutedEventArgs e)
        {
            if (Property != null && Property.HasAttribute<PropertyGridFocusAttribute>())
            {
                Focus();
            }
        }

#if !SILVERLIGHT
        new 
#endif
        protected virtual void Focus()
        {
            
        }

        protected abstract UIElement CreateEditor();

        public TextBlock Label { get; set; }

        PropertyInfo mProperty;
        public PropertyInfo Property
        {
            get
            {
                return mProperty;
            }
            set
            {
                mProperty = value;
                if (mProperty != null)
                {
                    OnPropertySet();
                }
            }
        }

        protected object GetValue()
        {
            return Property.GetValue(Target, null);
        }

        protected T GetValue<T>()
        {
            return (T)GetValue();
        }

        protected virtual void SetValue(object value)
        {
            if (Target != null && Property != null)
            {
                IFigure figure = Target as IFigure;
                if (figure != null)
                {
                    SetPropertyAction action = new SetPropertyAction(
                        Target,
                        Property,
                        value);
                    figure.Drawing.ActionManager.RecordAction(action);
                }
                else
                {
                    Property.SetValue(Target, value, null);
                }
            }
        }

        protected virtual void OnPropertySet()
        {
            if (mProperty.HasAttribute<PropertyGridNameAttribute>())
            {
                Label.Text = mProperty.GetAttribute<PropertyGridNameAttribute>().Name;
                return;
            }
            Label.Text = mProperty.Name;
        }

        public object Target { get; set; }
    }
}
