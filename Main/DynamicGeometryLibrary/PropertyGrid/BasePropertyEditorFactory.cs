﻿using System.Reflection;

namespace DynamicGeometry
{
    public abstract class BasePropertyEditorFactory<T> : IPropertyEditorFactory
        where T : IPropertyEditor, new()
    {
        public virtual bool SupportsProperty(PropertyInfo property)
        {
            return property.CanRead
                && property.CanWrite;
        }

        public virtual IPropertyEditor CreateEditor(PropertyInfo property, object target)
        {
            var editor = new T() { Property = property, Target = target };
            editor.Init();
            return editor;
        }
    }
}
