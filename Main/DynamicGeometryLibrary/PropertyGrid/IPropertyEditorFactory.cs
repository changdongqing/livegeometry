﻿using System.Reflection;

namespace DynamicGeometry
{
    public interface IPropertyEditorFactory
    {
        bool SupportsProperty(PropertyInfo property);
        IPropertyEditor CreateEditor(PropertyInfo property, object target);
    }
}
