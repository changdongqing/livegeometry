﻿using System.Windows;
using System.Windows.Controls;

namespace DynamicGeometry
{
    public class ReadOnlyValuePropertyEditorFactory : BasePropertyEditorFactory<ReadOnlyValuePropertyEditor>
    {
        public override bool SupportsProperty(System.Reflection.PropertyInfo property)
        {
            return property.CanRead
                && !property.CanWrite;
        }
    }

    public class ReadOnlyValuePropertyEditor : LabeledPropertyEditor, IPropertyEditor
    {
        public TextBlock TextBlock { get; set; }

        protected override UIElement CreateEditor()
        {
            TextBlock = new TextBlock();
            TextBlock.VerticalAlignment = VerticalAlignment.Center;
            return TextBlock;
        }

        public void Init()
        {
            var value = Property.GetValue(Target, null);
            TextBlock.Text = (value ?? "").ToString();
        }
    }
}
