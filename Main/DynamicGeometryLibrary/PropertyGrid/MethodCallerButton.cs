﻿using System.Reflection;
using System.Windows.Controls;

namespace DynamicGeometry
{
    public class MethodCallerButton : Button
    {
        public MethodCallerButton()
        {
            this.Click += MethodCallerButton_Click;
            this.Margin = new System.Windows.Thickness(0, 4, 0, 4);
        }

        void MethodCallerButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (Target != null && Method != null)
            {
                try
                {
                    Method.Invoke(Target, new object[] { });
                }
                catch
                {
                    // whatever happens, we can't allow it bubble up
                    // back to the CLR - we don't trust our method
                }
            }
        }

        public object Target { get; set; }

        private MethodInfo mMethod;
        public MethodInfo Method
        {
            get
            {
                return mMethod;
            }
            set
            {
                mMethod = value;
                if (mMethod != null)
                {
                    this.Content = mMethod.Name;
                }
            }
        }
    }
}
