﻿using System.Windows;
using System.Windows.Controls;

namespace DynamicGeometry
{
    public class BooleanPropertyEditorFactory : BasePropertyEditorFactory<BooleanPropertyEditor>
    {
        public override bool SupportsProperty(System.Reflection.PropertyInfo property)
        {
            return property.PropertyType == typeof(bool)
                && base.SupportsProperty(property);
        }
    }

    public class BooleanPropertyEditor : LabeledPropertyEditor, IPropertyEditor
    {
        public CheckBox CheckBox { get; set; }

        protected override UIElement CreateEditor()
        {
            CheckBox = new CheckBox();
            CheckBox.VerticalAlignment = VerticalAlignment.Center;
            CheckBox.Checked += CheckBox_CheckedChanged;
            CheckBox.Unchecked += CheckBox_CheckedChanged;
            return CheckBox;
        }

        bool guard = false;
        void CheckBox_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (guard)
            {
                return;
            }
            SetValue(CheckBox.IsChecked ?? true);
        }

        public void Init()
        {
            guard = true;
            CheckBox.IsChecked = GetValue<bool>();
            guard = false;
        }
    }
}
