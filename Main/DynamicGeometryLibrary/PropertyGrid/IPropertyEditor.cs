﻿using System.Reflection;

namespace DynamicGeometry
{
    public interface IPropertyEditor
    {
        PropertyInfo Property { get; set; }
        object Target { get; set; }
        void Init();
    }
}
