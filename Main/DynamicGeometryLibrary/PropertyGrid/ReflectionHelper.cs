﻿using System;
using System.Linq;
using System.Reflection;

namespace DynamicGeometry
{
    public static class ReflectionHelper
    {
        public static EventInfo FindEventByName(Type type, string eventName)
        {
            var events = type.GetEvents();
            return events.FirstOrDefault(e => e.Name == eventName);
        }

        public static MethodInfo FindMethodByName(Type type, string methodName)
        {
            var methods = type.GetMethods();
            return methods.FirstOrDefault(m => m.Name == methodName);
        }
    }
}