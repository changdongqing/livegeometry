﻿using System.Windows;
using System.Windows.Controls;

namespace DynamicGeometry
{
    public class CoordinatesMeasurementCreator : FigureCreator
    {
        protected override DependencyList InitExpectedDependencies()
        {
            return DependencyList.Point;
        }

        protected override IFigure CreateFigure()
        {
            var result = Factory.CreateCoordinatesMeasurement(Drawing, FoundDependencies);
            return result;
        }

        protected override void CreateAndAddFigure()
        {
            var alreadyExisting = Drawing.Figures.FindFigureWithTheseDependencies
                <CoordinatesMeasurement>
                (FoundDependencies[0]);
            if (alreadyExisting != null)
            {
                Drawing.Remove(alreadyExisting);
            }
            else
            {
                base.CreateAndAddFigure();
            }
        }

        public override string Name
        {
            get { return "Coordinates"; }
        }

        public override string HintText
        {
            get
            {
                return "Click on a point to show or hide its coordinates.";
            }
        }

        public override System.Windows.Controls.Panel CreateIcon()
        {
            var grid = new Grid() { Width = Behavior.IconSize, Height = Behavior.IconSize };
            var icon = new StackPanel();
            var text = new TextBlock() { Text = "(X;Y)" };
            icon.HorizontalAlignment = HorizontalAlignment.Center;
            icon.VerticalAlignment = VerticalAlignment.Center;
            icon.Children.Add(text);
            icon.Children.Add(Factory.CreatePointShape());
            grid.Children.Add(icon);
            return grid;
        }
    }
}