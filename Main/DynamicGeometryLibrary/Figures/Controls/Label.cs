﻿using System.Windows;
using System.Windows.Media;
using System.Xml.Linq;

namespace DynamicGeometry
{
    public class Label : LabelBase, IMovable
    {
        public override void UpdateVisual()
        {
            if (!Dependencies.IsEmpty())
            {
                MoveToCore(Point(0));
            }
            base.UpdateVisual();
        }

        public override void ReadXml(XElement element)
        {
            Text = element.ReadString("Text").Replace(@"\n", "\n");
            var x = element.ReadDouble("X");
            var y = element.ReadDouble("Y");
            MoveTo(new Point(x, y));

            var foreground = element.ReadString("Foreground");
            if (!foreground.IsEmpty())
            {
                Shape.Foreground = new SolidColorBrush(foreground.ToColor());
            }

            var fontSize = element.ReadDouble("FontSize");
            if (fontSize != 0)
            {
                Shape.FontSize = fontSize;
            }
        }

        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            var coordinates = Coordinates;
            writer.WriteAttributeString("Text", Text.Replace("\n", @"\n"));
            writer.WriteAttributeString("X", coordinates.X.ToStringInvariant());
            writer.WriteAttributeString("Y", coordinates.Y.ToStringInvariant());
        }
    }
}
