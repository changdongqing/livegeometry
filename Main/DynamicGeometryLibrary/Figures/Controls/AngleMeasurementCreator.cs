﻿using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Markup;
using M = System.Math;
using System.Windows.Controls;

namespace DynamicGeometry
{
    public class AngleMeasurementCreator : FigureCreator
    {
        protected override DependencyList InitExpectedDependencies()
        {
            return DependencyList.PointPointPoint;
        }

        protected override IFigure CreateFigure()
        {
            var result = Factory.CreateAngleMeasurement(Drawing, FoundDependencies);
            return result;
        }

        protected override void CreateAndAddFigure()
        {
            base.CreateAndAddFigure();
            Drawing.Add(Factory.CreateAngleArc(Drawing, FoundDependencies));
        }

        public override string Name
        {
            get { return "Angle"; }
        }

        public override string HintText
        {
            get
            {
                return "Click a point, a vertex, and another point to measure the angle.";
            }
        }

        public override System.Windows.Controls.Panel CreateIcon()
        {
            var builder = IconBuilder.BuildIcon();
            //    <Path.Data>
            //  <PathGeometry>
            //    <PathGeometry.Figures>
            //      <PathFigure StartPoint="10,50">
            //        <PathFigure.Segments>
            //          <BezierSegment
            //            Point1="100,0"
            //            Point2="200,200"
            //            Point3="300,100"/>
            //          <LineSegment Point="400,100" />
            //          <ArcSegment
            //            Size="50,50" RotationAngle="45"
            //            IsLargeArc="True" SweepDirection="Clockwise"
            //            Point="200,100"/>
            //        </PathFigure.Segments>
            //      </PathFigure>
            //    </PathGeometry.Figures>
            //  </PathGeometry>
            //</Path.Data>
            //var path = new Path()
            //{
            //    Data = new PathGeometry()
            //    {
            //        Figures = new PathFigureCollection()
            //        {
            //            new PathFigure()
            //            {
            //                Segments = new PathSegmentCollection()
            //                {
            //                    new BezierSegment()
            //                    {

            //                    }
            //                }
            //            }
            //        }
            //    }
            //}
            var size = Behavior.IconSize;
            var centerX = (size - 4) / (2 * size);
            var centerY = 1 - 4 / size;
            builder.Line(centerX, centerY, 0.8, 0.2)
                .Line(centerX, centerY, 1, centerY);

            var xaml = string.Format(@"
  <Path xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation'
    Stroke='Black'
    StrokeThickness='1'
    Fill='#33FF33'
    Data='m 0,{0} v-4 a {1},{1} 0 0 1 {2},0 v4 z m {4},-5 a 6,6 0 0 1 {3},0 z'
  />", size, (size - 4) / 2, size - 4, size / 2, (size - 4) / 4 - 1);
#if SILVERLIGHT
            var path = XamlReader.Load(xaml) as Path;
#else
            var path = XamlReader.Parse(xaml) as Path;
#endif
            builder.Canvas.Children.Add(path);

            //builder.Point(0.8, 0.2)
            //    .Point(1, centerY)
            //    .Point(centerX, centerY);

            var radius = ((size - 4) / 2) * 0.95;
            var radiusSmall = radius * 0.8;
            Point center = new Point(radius + 1, size - 4);
            for (double i = 0; i < 16; i++)
            {
                var angle = i * Math.PI / 15;
                builder.Line((center.X + radius * M.Cos(angle)) / size,
                    (center.Y - radius * M.Sin(angle)) / size,
                    (center.X + radiusSmall * M.Cos(angle)) / size,
                    (center.Y - radiusSmall * M.Sin(angle)) / size);
            }

            //var polygon = builder.AddPolygon(
            //    new Point(0.1, 0.8),
            //    new Point(0.3, 1),
            //    new Point(1, 0.3),
            //    new Point(0.8, 0.1)
            //);
            //polygon.Fill = new SolidColorBrush(Colors.Yellow);
            //polygon.Stroke = new SolidColorBrush(Colors.Black);
            //builder.Line(0, 0.7, 0.7, 0);
            //for (double i = 0.2; i <= 0.7; i += 0.1)
            //{
            //    builder.Line(i, 0.9 - i, i + 0.1, 1 - i);
            //}
            //for (double i = 0.15; i <= 0.75; i += 0.1)
            //{
            //    builder.Line(i, 0.9 - i, i + 0.05, 0.95 - i);
            //}

            return builder.Canvas;
        }
    }
}