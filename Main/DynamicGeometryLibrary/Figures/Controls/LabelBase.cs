﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.Windows.Media;

namespace DynamicGeometry
{
    public abstract class LabelBase : CoordinatesShapeBase<TextBlock>
    {
        protected override int DefaultZOrder()
        {
            return (int)ZOrder.Labels;
        }

        protected override TextBlock CreateShape()
        {
            return Factory.CreateLabelShape();
        }

        [PropertyGridVisible]
        public string Text
        {
            get
            {
                return Shape.Text;
            }
            set
            {
                Shape.Text = value;
            }
        }

        public override IFigure HitTest(Point point)
        {
            double left = Canvas.GetLeft(Shape);
            double top = Canvas.GetTop(Shape);
            point = ToPhysical(point);

            if (left <= point.X
                && left + Shape.ActualWidth >= point.X
                && top <= point.Y
                && top + Shape.ActualHeight >= point.Y)
            {
                return this;
            }
            return null;
        }

        protected override void UpdateShapeAppearance()
        {
            if (base.Selected)
            {
                Shape.Foreground = new SolidColorBrush(Colors.Red);
            }
            else
            {
                Shape.Foreground = new SolidColorBrush(Colors.Black);
            }
        }
    }
}
