﻿using System.Net;
using System.Windows;
using System.Windows.Controls;

namespace DynamicGeometry
{
    public class Hyperlink : CoordinatesShapeBase<HyperlinkButton>, IMovable
    {
        public Hyperlink()
        {
            Shape = CreateShape();
            ZIndex = (int)ZOrder.Labels;
            Shape.Click += Shape_Click;
            internet.DownloadStringCompleted += internet_DownloadStringCompleted;
        }

        WebClient internet = new WebClient();

        private string mUrl = null;
        public string Url
        {
            get
            {
                return mUrl;
            }
            set
            {
                mUrl = value;
                Shape.IsEnabled = false;
                internet.DownloadStringAsync(new System.Uri(value));
            }
        }

        void Shape_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(fileText))
            {
                Drawing.RaiseDocumentOpenRequested(new Drawing.DocumentOpenRequestedEventArgs()
                {
                    DocumentXml = fileText,
                    InWhichWindow = Drawing.DocumentOpenRequestedEventArgs.InWhichWindowChoice.DontCare
                });
            }
        }

        string fileText = null;

        void internet_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            if (e.Cancelled || e.Error != null)
            {
                if (e.Cancelled)
                {
                    Shape.Content = "Cancelled";
                }
                if (e.Error != null)
                {
                    Shape.Content = "Error: " + e.Error.ToString();
                }
                return;
            }
            fileText = e.Result;
            Shape.IsEnabled = true;
        }

        protected override HyperlinkButton CreateShape()
        {
            return new HyperlinkButton()
            {
                FontSize = 24
            };
        }

        [PropertyGridVisible]
        public string Text
        {
            get
            {
                return Shape.Content.ToString();
            }
            set
            {
                Shape.Content = value;
            }
        }

        public override IFigure HitTest(Point point)
        {
            double left = Canvas.GetLeft(Shape);
            double top = Canvas.GetTop(Shape);
            point = ToPhysical(point);

            if (left <= point.X
                && left + Shape.ActualWidth >= point.X
                && top <= point.Y
                && top + Shape.ActualHeight >= point.Y)
            {
                return this;
            }
            return null;
        }

        public override void ReadXml(System.Xml.Linq.XElement element)
        {
            base.ReadXml(element);
            Url = element.ReadString("Url");
            Text = element.ReadString("Text");
            var x = element.ReadDouble("X");
            var y = element.ReadDouble("Y");
            this.MoveTo(x, y);
        }

        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            base.WriteXml(writer);
            writer.WriteAttributeString("Url", Url);
            writer.WriteAttributeString("Text", Text);
            writer.WriteAttributeDouble("X", Coordinates.X);
            writer.WriteAttributeDouble("Y", Coordinates.Y);
        }
    }
}