﻿using System.Windows;

namespace DynamicGeometry
{
    public class AreaMeasurement : MeasurementBase
    {
        public override void UpdateVisual()
        {
            var points = Dependencies.ToPoints();
            MoveToCore(points.Midpoint());
            base.UpdateVisual();
            Shape.Text = points.Area().ToString("F01");
        }
    }
}
