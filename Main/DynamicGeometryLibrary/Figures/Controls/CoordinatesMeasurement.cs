﻿using System.Windows;

namespace DynamicGeometry
{
    public class CoordinatesMeasurement : MeasurementBase
    {
        public override void MoveToCore(Point newPosition)
        {
            Point newOffset = newPosition.Minus(Point(0));
            newOffset = newOffset.TrimToMaxLength(ToLogical(100));
            Offset = newOffset;
            base.MoveToCore(newPosition);
        }

        public override void UpdateVisual()
        {
            var coords = Point(0);
            MoveToCore(coords.Plus(Offset));
            base.UpdateVisual();
            Shape.Text = string.Format("({0:0.#};{1:0.#})",
                coords.X,
                coords.Y);
        }
    }
}
