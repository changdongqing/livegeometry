﻿using System.Windows;

namespace DynamicGeometry
{
    public class AngleMeasurement : MeasurementBase
    {
        public override void MoveToCore(Point newPosition)
        {
            Point newOffset = newPosition.Minus(Point(1));
            Offset = newOffset;
            base.MoveToCore(newPosition);
        }

        public override void UpdateVisual()
        {
            var p = Point(1).Plus(Offset);
            MoveToCore(p);
            base.UpdateVisual();
            Shape.Text = string.Format(
                "{0:F0}°",
                (Math.OAngle(Point(0), Point(1), Point(2)) * 180 / Math.PI));
        }
    }
}
