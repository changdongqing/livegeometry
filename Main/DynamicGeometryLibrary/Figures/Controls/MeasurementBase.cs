﻿using System.Windows;

namespace DynamicGeometry
{
    public abstract class MeasurementBase : LabelBase, IMovable
    {
        protected Point Offset;
    }
}
