﻿using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;

namespace DynamicGeometry
{
    public class AreaMeasurementCreator : FigureCreator
    {
        protected override void Click(Point coordinates)
        {
            var polygon = Drawing.Figures.HitTest<Polygon>(coordinates);
            if (polygon != null)
            {
                RemoveIntermediateFigureIfNecessary();
                if (TempPoint != null)
                {
                    FoundDependencies.Remove(TempPoint);
                    Drawing.Figures.Remove(TempPoint);
                    TempPoint = null;
                }
                FoundDependencies.Clear();
                FoundDependencies = new FigureList(polygon.Dependencies);
                Finish();
                return;
            }

            var point = Drawing.Figures.HitTest<IPoint>(coordinates);
            if (point != null
                && !FoundDependencies.IsEmpty()
                && FoundDependencies.Count >= 4 // 4 including the TempPoint 
                                                // (and 3 after TempPoint is removed)
                && FoundDependencies[0] == point)
            {
                RemoveIntermediateFigureIfNecessary();
                if (TempPoint != null)
                {
                    FoundDependencies.Remove(TempPoint);
                    Drawing.Figures.Remove(TempPoint);
                    TempPoint = null;
                }
                Finish();
                return;
            }
            base.Click(coordinates);
        }

        protected override DependencyList InitExpectedDependencies()
        {
            return null;
        }

        protected override System.Type ExpectedDependency
        {
            get
            {
                return typeof(IPoint);
            }
        }

        protected override bool CanReuseDependency()
        {
            return true;
        }

        protected override IFigure CreateFigure()
        {
            return Factory.CreateAreaMeasurement(Drawing, FoundDependencies);
        }

        protected override IFigure CreateIntermediateFigure()
        {
            if (!FoundDependencies.All(f => f is IPoint))
            {
                return null;
            }
            else if (FoundDependencies.Count >= 3)
            {
                var result = Factory.CreateAreaMeasurement(Drawing, FoundDependencies);
                return result;
            }
            return null;
        }

        public override string Name
        {
            get { return "Area"; }
        }

        public override string HintText
        {
            get
            {
                return "Click a polygon or a list of points to measure its area.";
            }
        }

        public override System.Windows.Controls.Panel CreateIcon()
        {
            var builder = IconBuilder.BuildIcon();
            var polygon = builder.AddPolygon(
                new Point(0.2, 0.4),
                new Point(0.3, 0.8),
                new Point(0.7, 0.8),
                new Point(0.8, 0.4),
                new Point(0.6, 0.2)
            );
            polygon.Fill = new SolidColorBrush(Color.FromArgb(255, 255, 200, 200));
            polygon.Stroke = new SolidColorBrush(Colors.Black);
            return builder.Canvas;
        }
    }
}