﻿using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace DynamicGeometry
{
    public class AngleArc : ArcBase
    {
        public AngleArc() : base()
        {
            Size = 16;
            ArcShape.Size = new Size(this.Size, this.Size);
        }

        public double Size { get; set; }

        public override IFigure HitTest(Point point)
        {
            return null;
        }

        public override void UpdateVisual()
        {
            var center = Point(1);
            var logicalSize = ToLogical(Size);
            var distance1 = center.Distance(Point(0));
            var distance2 = center.Distance(Point(2));
            if (distance1 == 0 || distance2 == 0)
            {
                Shape.Visibility = Visibility.Collapsed;
                return;
            }

            var startPoint = Math.ScalePointBetweenTwo(
                center, 
                Point(0), 
                logicalSize / distance1);
            var endPoint = Math.ScalePointBetweenTwo(
                center,
                Point(2),
                logicalSize / distance2);

            Figure.StartPoint = ToPhysical(startPoint);
            ArcShape.Point = ToPhysical(endPoint);

            ArcShape.IsLargeArc = Math.OAngle(startPoint, center, endPoint) > Math.PI;

            if (Shape.Visibility != Visibility.Visible)
            {
                Shape.Visibility = Visibility.Visible;
            }
        }
    }
}
