﻿using System.Windows;

namespace DynamicGeometry
{
    public class DistanceMeasurement : MeasurementBase
    {
        public override void MoveToCore(Point newPosition)
        {
            Offset = newPosition.Minus(Midpoint());
            base.MoveToCore(newPosition);
        }

        public Point Midpoint()
        {
            return Math.Midpoint(Point(0), Point(1));
        }

        public override void UpdateVisual()
        {
            var p = Midpoint().Plus(Offset);
            MoveToCore(p);
            base.UpdateVisual();
            Shape.Text = Point(0).Distance(Point(1)).ToString("F01");
        }
    }
}
