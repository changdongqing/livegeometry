﻿using System.Windows;
using System.Windows.Shapes;

namespace DynamicGeometry
{
    public abstract class CircleBase : ShapeBase<Shape>, ICircle
    {
        public string c_store_id { get; set; }
        public string c_guid { get; set; }
        public string c_type { get; set; }
        public bool c_is_modify { get; set; }

        public abstract Point Center
        {
            get;
        }

        public abstract double Radius
        {
            get;
        }

        public override IFigure HitTest(Point point)
        {
            if ((Center.Distance(point) - Radius).Abs() < CursorTolerance)
            {
                return this;
            }
            return null;
        }

        public override void UpdateVisual()
        {
            var center = ToPhysical(Center);
            var diameter = ToPhysical(Radius * 2);
            Shape.Width = diameter;
            Shape.Height = diameter;
            Shape.CenterAt(center);
        }

        public virtual double GetNearestParameterFromPoint(Point point)
        {
            return Math.GetAngle(point, Center);
        }

        public virtual Point GetPointFromParameter(double parameter)
        {
            var center = Center;
            var radius = Radius;
            return new Point(
                center.X + radius * System.Math.Cos(parameter),
                center.Y + radius * System.Math.Sin(parameter));
        }

        public virtual Tuple<double, double> GetParameterDomain()
        {
            return Tuple.Create(0.0, 2 * DynamicGeometry.Math.PI);
        }

        protected override Shape CreateShape()
        {
            return Factory.CreateCircleShape();
        }
    }
}
