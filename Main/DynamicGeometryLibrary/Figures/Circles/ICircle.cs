﻿using System.Windows;

namespace DynamicGeometry
{
    public interface ICircle : IFigure, ILinearFigure
    {
        Point Center { get; }
        double Radius { get; }
    }
}