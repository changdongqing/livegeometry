﻿using System.Windows.Media;

namespace DynamicGeometry
{
    public class ArcCreator : FigureCreator
    {
        protected override IFigure CreateFigure()
        {
            return Factory.CreateArc(Drawing, FoundDependencies);
        }

        protected override DependencyList InitExpectedDependencies()
        {
            return DependencyList.PointPointPoint;
        }

        protected override IFigure CreateIntermediateFigure()
        {
            if (FoundDependencies.Count == 2
                && FoundDependencies[0] is IPoint
                && FoundDependencies[1] is IPoint)
            {
                return Factory.CreateSegment(Drawing, FoundDependencies);
            }
            return null;
        }

        public override string Name
        {
            get
            {
                return "Arc";
            }
        }

        public override string HintText
        {
            get
            {
                return "Click (and release) the center point and then two points (start and end of the arc, counterclockwise).";
            }
        }

        public override System.Windows.Media.Color ToolButtonColor
        {
            get
            {
                return Color.FromArgb(255, 180, 130, 255);
            }
        }

        protected override bool CanReuseDependency()
        {
            return FoundDependencies.Count == 3;
        }

        public override System.Windows.Controls.Panel CreateIcon()
        {
            const double r = 0.4;
            return IconBuilder.BuildIcon()
                .Arc(0.5, 0.5, 0.5 + r, 0.5, 0.5, 0.5 - r)
                .Point(0.5, 0.5 - r)
                .Point(0.5, 0.5)
                .Point(0.5 + r, 0.5)
                .Canvas;
        }
    }
}