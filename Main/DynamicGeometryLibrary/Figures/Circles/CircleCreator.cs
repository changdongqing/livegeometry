﻿using System.Windows.Media;

namespace DynamicGeometry
{
    public class CircleCreator : FigureCreator
    {
        protected override IFigure CreateFigure()
        {
            return Factory.CreateCircle(Drawing, FoundDependencies);
        }

        protected override DependencyList InitExpectedDependencies()
        {
            return DependencyList.PointPoint;
        }

        public override string Name
        {
            get
            {
                return "Circle";
            }
        }

        public override string HintText
        {
            get
            {
                return "Click the circle center and then click a point on a circle.";
            }
        }

        public override System.Windows.Media.Color ToolButtonColor
        {
            get
            {
                return Color.FromArgb(255, 255, 130, 120);
            }
        }

        public override System.Windows.Controls.Panel CreateIcon()
        {
            return IconBuilder.BuildIcon()
                .Circle(0.5, 0.5, 0.5)
                .Point(0.5, 0.5)
                .Point(0.85, 0.15)
                .Canvas;
        }
    }
}