﻿using System.Windows.Media;

namespace DynamicGeometry
{
    public class CircleByRadiusCreator : FigureCreator
    {
        protected override IFigure CreateFigure()
        {
            return Factory.CreateCircleByRadius(Drawing, FoundDependencies);
        }

        protected override DependencyList InitExpectedDependencies()
        {
            return DependencyList.PointPointPoint;
        }

        protected override IFigure CreateIntermediateFigure()
        {
            if (FoundDependencies.Count == 2
                && FoundDependencies[0] is IPoint
                && FoundDependencies[1] is IPoint)
            {
                return Factory.CreateSegment(Drawing, FoundDependencies);
            }
            return null;
        }

        public override string Name
        {
            get
            {
                return "By Radius";
            }
        }

        public override string HintText
        {
            get
            {
                return "Click (and release) two points (start and end of a radius) and then click the circle center.";
            }
        }

        public override System.Windows.Media.Color ToolButtonColor
        {
            get
            {
                return Color.FromArgb(255, 255, 120, 240);
            }
        }

        protected override bool CanReuseDependency()
        {
            return FoundDependencies.Count == 3;
        }

        public override System.Windows.Controls.Panel CreateIcon()
        {
            const double r = 0.4;
            return IconBuilder.BuildIcon()
                .Circle(r, r, r)
                .Line(0.5, 0.9, 0.5 + r, 0.9)
                .Point(r, r)
                .Point(0.5, 0.9)
                .Point(0.5 + r, 0.9)
                .Canvas;
        }
    }
}