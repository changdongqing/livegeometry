﻿using System.Windows;
using System.Windows.Shapes;

namespace DynamicGeometry
{
    public class Circle : CircleBase
    {
        public override Point Center
        {
            get { return Point(0); }
        }

        public override double Radius
        {
            get { return Center.Distance(Point(1)); }
        }
    }
}
