﻿using System.Windows.Media;

namespace DynamicGeometry
{
    public class ParallelLineCreator : FigureCreator
    {
        protected override IFigure CreateFigure()
        {
            return Factory.CreateParallelLine(Drawing, FoundDependencies);
        }

        protected override DependencyList InitExpectedDependencies()
        {
            return DependencyList.LinePoint;
        }

        public override string Name
        {
            get
            {
                return "Parallel";
            }
        }

        public override string HintText
        {
            get
            {
                return "Click a line and then click a point.";
            }
        }

        public override System.Windows.Media.Color ToolButtonColor
        {
            get
            {
                return Color.FromArgb(255, 128, 255, 255);
            }
        }

        public override System.Windows.Controls.Panel CreateIcon()
        {
            return IconBuilder.BuildIcon()
                .Line(0, 0.7, 0.7, 0)
                .Line(0.3, 1, 1, 0.3)
                .Point(0.35, 0.35)
                .Canvas;
        }
    }
}