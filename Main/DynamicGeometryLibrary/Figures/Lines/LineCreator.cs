﻿using System.Windows.Media;

namespace DynamicGeometry
{
    public class LineTwoPointsCreator : FigureCreator
    {
        protected override IFigure CreateFigure()
        {
            return Factory.CreateLineTwoPoints(Drawing, FoundDependencies);
        }

        protected override DependencyList InitExpectedDependencies()
        {
            return DependencyList.PointPoint;
        }

        public override string Name
        {
            get
            {
                return "Line";
            }
        }

        public override string HintText
        {
            get
            {
                return "Click (and release) twice to draw a line between two points.";
            }
        }

        public override System.Windows.Media.Color ToolButtonColor
        {
            get
            {
                return Color.FromArgb(255, 120, 255, 160);
            }
        }

        public override System.Windows.Controls.Panel CreateIcon()
        {
            return IconBuilder.BuildIcon()
                .Line(0, 1, 1, 0)
                .Point(0.25, 0.75)
                .Point(0.75, 0.25)
                .Canvas;
        }
    }
}