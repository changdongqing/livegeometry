﻿namespace DynamicGeometry
{
    public class Segment : LineBase, ILine
    {
        public string c_store_id { get; set; }
        public string c_guid { get; set; }

        public string c_type { get; set; }
        public bool c_is_modify { get; set; }
        [PropertyGridVisible]
        public double Length
        {
            get
            {
                return Coordinates.Length;
            }
        }

        public override double GetNearestParameterFromPoint(System.Windows.Point point)
        {
            var parameter = base.GetNearestParameterFromPoint(point);
            if (parameter < 0)
            {
                parameter = 0;
            }
            else if (parameter > 1)
            {
                parameter = 1;
            }
            return parameter;
        }

        public override IFigure HitTest(System.Windows.Point point)
        {
            var boundingRect = Coordinates.GetBoundingRect().Inflate(CursorTolerance);
            return boundingRect.Contains(point) ? base.HitTest(point) : null;
        }

        public override Tuple<double, double> GetParameterDomain()
        {
            return Tuple.Create(0.0, 1.0);
        }
    }
}