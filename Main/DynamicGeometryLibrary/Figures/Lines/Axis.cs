﻿using System.Collections.Generic;
using System.Windows.Media;

namespace DynamicGeometry
{
    public class Axis : CompositeFigure
    {
        public Axis()
        {
            Line = new LineTwoPoints();
            Line.Shape.Stroke = new SolidColorBrush(Color);
            Line.Shape.StrokeThickness = 0.5;
            Line.SetZIndex(ZOrder.Axes);

            Arrow = new Arrow();
            Arrow.Dependencies = new IFigure[] { Line };
            Arrow.Shape.Fill = new SolidColorBrush(Color);
            Arrow.Shape.Stroke = null;

            Add(Line, Arrow);
        }

        public LineTwoPoints Line { get; set; }
        public Arrow Arrow { get; set; }

        public override bool Visible
        {
            get
            {
                return base.Visible;
            }
            set
            {
                base.Visible = value;
                Line.Visible = value;
                Arrow.Visible = value;
            }
        }

        public override IFigure HitTest(System.Windows.Point point, System.Predicate<IFigure> filter)
        {
            return null;
        }

        public static Color Color
        {
            get
            {
                return Color.FromArgb(255, 128, 128, 255);
            }
        }
    }
}
