﻿using System.Windows.Media;

namespace DynamicGeometry
{
    public class SegmentCreator : FigureCreator
    {
        protected override DependencyList InitExpectedDependencies()
        {
            return DependencyList.PointPoint;
        }

        protected override IFigure CreateFigure()
        {
            return Factory.CreateSegment(Drawing, FoundDependencies);
        }

        public override string Name
        {
            get { return "Segment"; }
        }

        public override string HintText
        {
            get
            {
                return "Click (and release) twice to connect two points with a segment.";
            }
        }

        public override System.Windows.Media.Color ToolButtonColor
        {
            get
            {
                return Color.FromArgb(255, 205, 255, 128);
            }
        }

        public override System.Windows.Controls.Panel CreateIcon()
        {
            return IconBuilder.BuildIcon()
                .Line(0.25, 0.75, 0.75, 0.25)
                .Point(0.25, 0.75)
                .Point(0.75, 0.25)
                .Canvas;
        }
    }
}