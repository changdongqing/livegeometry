﻿namespace DynamicGeometry
{
    public class PerpendicularLine : LineBase, ILine
    {
        public string c_store_id { get; set; }
        public string c_guid { get; set; }

        public string c_type { get; set; }
        public bool c_is_modify { get; set; }
        public override PointPair Coordinates
        {
            get
            {
                PointPair coordinates;
                PointPair parentLine = Line(0);
                System.Windows.Point point = Point(1);

                coordinates = new PointPair()
                {
                    P1 = point,
                    P2 =
                    {
                        X = point.X + parentLine.P2.Y - parentLine.P1.Y,
                        Y = point.Y + parentLine.P1.X - parentLine.P2.X
                    }
                };
                return coordinates;
            }
        }

        public override PointPair OnScreenCoordinates
        {
            get
            {
                return Math.GetLineFromSegment(Coordinates, CanvasLogicalBorders);
            }
        }
    }
}