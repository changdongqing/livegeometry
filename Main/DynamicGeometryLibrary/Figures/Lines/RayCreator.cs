﻿using System.Windows.Media;

namespace DynamicGeometry
{
    public class RayCreator : FigureCreator
    {
        protected override DependencyList InitExpectedDependencies()
        {
            return DependencyList.PointPoint;
        }

        protected override IFigure CreateFigure()
        {
            return Factory.CreateRay(Drawing, FoundDependencies);
        }

        public override string Name
        {
            get { return "Ray"; }
        }

        public override string HintText
        {
            get
            {
                return "Click (and release) twice to create a ray.";
            }
        }

        public override System.Windows.Media.Color ToolButtonColor
        {
            get
            {
                return Color.FromArgb(255, 160, 255, 128);
            }
        }

        public override System.Windows.Controls.Panel CreateIcon()
        {
            return IconBuilder.BuildIcon()
                .Line(0.25, 0.75, 1, 0)
                .Point(0.25, 0.75)
                .Point(0.75, 0.25)
                .Canvas;
        }
    }
}