﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace DynamicGeometry
{
    public abstract class LineDecorator : FigureDecorator<LineBase>, ILine
    {
        public string c_store_id { get; set; }
        public string c_guid { get; set; }

        public string c_type { get; set; }
        public bool c_is_modify { get; set; }
        public PointPair Coordinates
        {
            get { return Decorated.Coordinates; }
        }

        public double GetNearestParameterFromPoint(Point point)
        {
            return Decorated.GetNearestParameterFromPoint(point);
        }

        public Point GetPointFromParameter(double parameter)
        {
            return Decorated.GetPointFromParameter(parameter);
        }

        public Tuple<double, double> GetParameterDomain()
        {
            return Decorated.GetParameterDomain();
        }
    }
}
