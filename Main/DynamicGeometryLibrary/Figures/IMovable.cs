﻿using System.Windows;

namespace DynamicGeometry
{
    public interface IMovable
    {
        void MoveTo(Point position);
        Point Coordinates { get; }
    }

    public static class IMovableExtensions
    {
        public static void MoveTo(this IMovable movable, Point newPosition, Drawing drawing)
        {
            MoveAction action = new MoveAction(drawing, new[]{ movable }, newPosition.Minus(movable.Coordinates), null);
            drawing.ActionManager.RecordAction(action);
        }

        public static void MoveTo(this IMovable movable, double x, double y)
        {
            movable.MoveTo(new Point(x, y));
        }
    }
}
