﻿using System.Windows;

namespace DynamicGeometry
{
    public class SymmPoint : PointBase, IPoint
    {
        protected override System.Windows.Shapes.Shape CreateShape()
        {
            return Factory.CreateDependentPointShape();
        }

        public override void Recalculate()
        {
            Coordinates = new Point(
                2 * Point(1).X - Point(0).X,
                2 * Point(1).Y - Point(0).Y);
        }

        public string c_store_id { get; set; }
        public string c_guid { get; set; }

        public string c_type { get; set; }

        public bool c_is_modify { get; set; }
    }
}