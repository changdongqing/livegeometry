﻿using System.Windows.Media;

namespace DynamicGeometry
{
    public class SymmPointCreator : FigureCreator
    {
        protected override DependencyList InitExpectedDependencies()
        {
            return DependencyList.PointPoint;
        }

        protected override IFigure CreateFigure()
        {
            SymmPoint result = Factory.CreateSymmPoint(Drawing, FoundDependencies);
            return result;
        }

        public override string Name
        {
            get { return "Symm. point"; }
        }

        public override string HintText
        {
            get
            {
                return "Click a point and then click the symmetry center to create a symmetrical point.";
            }
        }

        public override System.Windows.Media.Color ToolButtonColor
        {
            get
            {
                return Color.FromArgb(255, 160, 150, 255);
            }
        }

        public override System.Windows.Controls.Panel CreateIcon()
        {
            return IconBuilder.BuildIcon()
                .Point(0.25, 0.75)
                .Point(0.5, 0.5)
                .DependentPoint(0.75, 0.25)
                .Canvas;
        }
    }
}