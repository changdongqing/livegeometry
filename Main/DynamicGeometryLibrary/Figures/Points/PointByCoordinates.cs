﻿using System;

namespace DynamicGeometry
{
    public class PointByCoordinates : PointBase
    {
        public Func<double> XValueProvider { get; set; }
        public Func<double> YValueProvider { get; set; }

        public override void Recalculate()
        {
            if (Dependencies.IsEmpty())
            {
                Coordinates = new System.Windows.Point(
                    XValueProvider(), YValueProvider());
            }
            else
            {
                Coordinates = new System.Windows.Point(
                    Number(0), Number(1));
            }
        }

        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            var coordinates = Coordinates;
            writer.WriteAttributeString("X", coordinates.X.ToStringInvariant());
            writer.WriteAttributeString("Y", coordinates.Y.ToStringInvariant());
        }
    }
}
