﻿using System.Windows;
using System.Xml.Linq;

namespace DynamicGeometry
{
    public class FreePoint : PointBase, IMovable
    {
        public override void ReadXml(XElement element)
        {
            var x = element.ReadDouble("X");
            var y = element.ReadDouble("Y");
            Coordinates = new Point(x, y);
        }

        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            var coordinates = Coordinates;
            writer.WriteAttributeDouble("X", coordinates.X);
            writer.WriteAttributeDouble("Y", coordinates.Y);
        }

        [PropertyGridVisible]
        public override double X
        {
            get
            {
                return base.X;
            }
            set
            {
                base.X = value;
            }
        }

        [PropertyGridVisible]
        public override double Y
        {
            get
            {
                return base.Y;
            }
            set
            {
                base.Y = value;
            }
        }
    }
}
