﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Media;

namespace DynamicGeometry
{
    public class FreePointCreator : Behavior
    {
        protected override void MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var coordinates = Coordinates(e);
            var list = Drawing.Figures.HitTestMany(coordinates);
            var figureList = list.Where(f => f is ILinearFigure).ToArray();
            if (!figureList.IsEmpty())
            {
                if (figureList.Length == 2)
                {
                    var intersection =
                        Factory.CreateIntersectionPoint(Drawing, figureList[0], figureList[1], coordinates);
                    Drawing.Add(intersection);
                }
                else if (figureList.Length == 1)
                {
                    var pointOnFigure =
                        Factory.CreatePointOnFigure(Drawing, figureList[0], coordinates);
                    Drawing.Add(pointOnFigure);
                }
            }
            else
            {
                CreatePointAtCurrentPosition(coordinates, true);
            }
        }

        public override string Name
        {
            get { return "Point"; }
        }

        public override string HintText
        {
            get { return "Click to create a point. You can also click on a figure or an intersection."; }
        }

        public override System.Windows.Media.Color ToolButtonColor
        {
            get { return Color.FromArgb(255, 255, 255, 128); }
        }
    }
}