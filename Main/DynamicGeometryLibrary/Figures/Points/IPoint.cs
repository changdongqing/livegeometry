﻿using System.Windows;

namespace DynamicGeometry
{
    public interface IPoint : IFigure
    {
        Point Coordinates { get; }
    }
}