﻿using System.Windows;

namespace DynamicGeometry
{
    public class MidPoint : PointBase, IPoint
    {
        public string c_store_id { get; set; }
        public string c_guid { get; set; }

        public string c_type { get; set; }
        public bool c_is_modify { get; set; }
        protected override System.Windows.Shapes.Shape CreateShape()
        {
            return Factory.CreateDependentPointShape();
        }

        public override void Recalculate()
        {
            Coordinates = new Point(
                (Point(0).X + Point(1).X) / 2,
                (Point(0).Y + Point(1).Y) / 2);
        }
    }
}