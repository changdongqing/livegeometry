﻿using System.Linq;
using System.Windows.Media;

namespace DynamicGeometry
{
    public class MidpointCreator : FigureCreator
    {
        protected override DependencyList InitExpectedDependencies()
        {
            return DependencyList.PointPoint;
        }

        protected override IFigure CreateFigure()
        {
            MidPoint result = Factory.CreateMidPoint(Drawing, FoundDependencies);
            return result;
        }

        protected override void MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var underMouse = Drawing.Figures.HitTest<Segment>(Coordinates(e));
            if (underMouse != null 
                && underMouse.Dependencies.Count() == 2
                && Drawing.Figures.HitTest<IPoint>(Coordinates(e)) == null)
            {
                FoundDependencies = new FigureList(underMouse.Dependencies);
            }
            base.MouseDown(sender, e);
        }

        public override string Name
        {
            get { return "Midpoint"; }
        }

        public override string HintText
        {
            get
            {
                return "Click two points (or a segment) to construct a midpoint.";
            }
        }

        public override System.Windows.Media.Color ToolButtonColor
        {
            get
            {
                return Color.FromArgb(255, 160, 160, 220);
            }
        }

        public override System.Windows.Controls.Panel CreateIcon()
        {
            return IconBuilder.BuildIcon()
                .Point(0.25, 0.75)
                .DependentPoint(0.5, 0.5)
                .Point(0.75, 0.25)
                .Canvas;
        }
    }
}