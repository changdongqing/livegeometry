﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace DynamicGeometry
{
    public abstract class PointBase : CoordinatesShapeBase<Shape>, IPoint
    {
        public string c_store_id { get; set; }
        public string c_guid { get; set; }

        public string c_type { get; set; }
        public bool c_is_modify { get; set; }
        protected override int DefaultZOrder()
        {
            return (int)ZOrder.Points;
        }

        protected override Shape CreateShape()
        {
            return Factory.CreatePointShape();
        }

        public override void UpdateVisual()
        {
            Shape.CenterAt(ToPhysical(Coordinates));
        }

        public virtual double X
        {
            get
            {
                return Coordinates.X;
            }
            set
            {
                Coordinates = Coordinates.SetX(value);
            }
        }

        public virtual double Y
        {
            get
            {
                return Coordinates.Y;
            }
            set
            {
                Coordinates = Coordinates.SetY(value);
            }
        }

        double PointSize
        {
            get
            {
                return Shape.ActualWidth / 2;
            }
        }

        public override IFigure HitTest(Point point)
        {
            double sensitivity = CursorTolerance + ToLogical(PointSize);
            if (point.X >= Coordinates.X - sensitivity
                && point.X <= Coordinates.X + sensitivity
                && point.Y >= Coordinates.Y - sensitivity
                && point.Y <= Coordinates.Y + sensitivity)
            {
                return this;
            }
            return null;
        }
    }
}