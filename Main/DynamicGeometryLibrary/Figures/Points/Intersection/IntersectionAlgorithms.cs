﻿using System.Windows;

namespace DynamicGeometry
{
    public class IntersectionAlgorithms
    {
        #region Line and Line

        public static Point IntersectLineAndLine(IFigure line1, IFigure line2)
        {
            return Math.GetIntersectionOfLines(
                ((ILine)line1).Coordinates,
                ((ILine)line2).Coordinates);
        }

        #endregion

        #region Circle and Line

        public static PointPair IntersectCircleAndLine(IFigure circle1, IFigure line1)
        {
            ICircle circle = (ICircle)circle1;
            ILine line = (ILine)line1;
            return Math.GetIntersectionOfCircleAndLine(
                circle.Center,
                circle.Radius,
                line.Coordinates);
        }

        public static Point IntersectCircleAndLine1(IFigure circle, IFigure line)
        {
            return IntersectCircleAndLine(circle, line).P1;
        }

        public static Point IntersectCircleAndLine2(IFigure circle, IFigure line)
        {
            return IntersectCircleAndLine(circle, line).P2;
        }

        public static PointPair IntersectLineAndCircle(IFigure line, IFigure circle)
        {
            return IntersectCircleAndLine(circle, line);
        }

        public static Point IntersectLineAndCircle1(IFigure line, IFigure circle)
        {
            return IntersectCircleAndLine1(circle, line);
        }

        public static Point IntersectLineAndCircle2(IFigure line, IFigure circle)
        {
            return IntersectCircleAndLine2(circle, line);
        }

        #endregion

        #region Circle and Circle

        public static PointPair IntersectCircleAndCircle(IFigure circle1, IFigure circle2)
        {
            ICircle c1 = (ICircle)circle1;
            ICircle c2 = (ICircle)circle2;
            return Math.GetIntersectionOfCircles(c1.Center, c1.Radius, c2.Center, c2.Radius);
        }

        public static Point IntersectCircleAndCircle1(IFigure circle1, IFigure circle2)
        {
            return IntersectCircleAndCircle(circle1, circle2).P1;
        }

        public static Point IntersectCircleAndCircle2(IFigure circle1, IFigure circle2)
        {
            return IntersectCircleAndCircle(circle1, circle2).P2;
        }

        #endregion
    }
}
