﻿namespace DynamicGeometry
{
    public enum ZOrder : int
    {
        Grid = 1,
        Axes = 2,
        Polygons = 3,
        Labels = 4,
        Figures = 5,
        Points = 6
    }
}
