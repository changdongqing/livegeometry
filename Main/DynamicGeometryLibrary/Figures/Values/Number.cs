﻿using System;

namespace DynamicGeometry
{
    public class Number : FigureBase, INumber
    {
        public string c_store_id { get; set; }
        public string c_guid { get; set; }

        public string c_type { get; set; }
        public bool c_is_modify { get; set; }
        public override IFigure HitTest(System.Windows.Point point)
        {
            return null;
        }

        public Func<double> Function { get; set; }

        public double Value
        {
            get
            {
                return Function();
            }
        }
    }
}
