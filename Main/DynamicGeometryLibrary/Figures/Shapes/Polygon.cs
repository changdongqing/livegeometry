﻿namespace DynamicGeometry
{
    public class Polygon : PolygonBase
    {
        public override void UpdateVisual()
        {
            Shape.Points = Drawing.CoordinateSystem
                .ToPhysical(Dependencies.ToPoints())
                .ToPointCollection();
        }
    }
}
