﻿using System;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Input;

namespace DynamicGeometry
{
    public class FunctionGraphCreator : Behavior
    {
        [PropertyGridName("Function graph")]
        public class Dialog
        {
            public Dialog(FunctionGraphCreator parent)
            {
                this.parent = parent;
            }

            FunctionGraphCreator parent;

            [PropertyGridVisible]
            [PropertyGridFocus]
            [PropertyGridEvent("KeyDown", "Func_KeyDown")]
            [PropertyGridName("f(x) = ")]
            public string Func { get; set; }

            internal void Func_KeyDown(object sender, KeyEventArgs e)
            {
                if (e.Key == System.Windows.Input.Key.Escape)
                {
                    Cancel();
                    e.Handled = true;
                }
                else if (e.Key == System.Windows.Input.Key.Enter)
                {
                    Plot();
                    e.Handled = true;
                }
            }

            [PropertyGridVisible]
            public void Plot()
            {
                if (ExpressionCompiler.Singleton != null)
                {
                    Func<double, double> func =
                        ExpressionCompiler.Singleton.Compile(Func);
                    if (func != null)
                    {
                        var graph = new FunctionGraph();
                        graph.Drawing = parent.Drawing;
                        graph.FunctionText = Func;
                        parent.Drawing.Add(graph);
                        Func = "";
                    }
                }
                Cancel();
            }

            [PropertyGridVisible]
            public void Cancel()
            {
                parent.AbortAndSetDefaultTool();
            }
        }

        public override object PropertyBag
        {
            get
            {
                if (PropertyDialog == null)
                {
                    PropertyDialog = new Dialog(this);
                }
                return PropertyDialog;
            }
        }

        Dialog PropertyDialog;

        protected override void MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            PropertyDialog.Cancel();
        }

        public override string Name
        {
            get { return "Function"; }
        }

        public override string HintText
        {
            get
            {
                return "Enter an expression that depends on x, such as sin(x) or x * x - 3";
            }
        }

        public override Color ToolButtonColor
        {
            get { return Color.FromArgb(255, 240, 240, 220); }
        }

        public override System.Windows.Controls.Panel CreateIcon()
        {
            var text = new TextBlock() { Text = "y=f(x)" };
            var result = IconBuilder.BuildIcon().Canvas;
            result.Children.Add(text);
            return result;
        }
    }
}
