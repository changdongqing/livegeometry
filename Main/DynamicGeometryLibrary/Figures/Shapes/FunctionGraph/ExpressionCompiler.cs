﻿using System;

namespace DynamicGeometry
{
    public abstract class ExpressionCompiler
    {
        public abstract Func<double, double> Compile(string expression);

        public static ExpressionCompiler Singleton { get; set; }
    }
}
