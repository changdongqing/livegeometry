﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;

namespace DynamicGeometry
{
    public class Locus : Curve
    {
        const int count = 30;

        public Locus()
        {
            for (int i = 0; i < count; i++)
            {
                pathSegments.Add(new LineSegment());
            }
        }

        public override IList<Point> GetPoints()
        {
            List<Point> result = new List<Point>(count);

            var point = Dependencies.ElementAt(0) as IPoint;
            var pointOnFigure = Dependencies.ElementAt(1) as PointOnFigure;
            var figure = pointOnFigure.LinearFigure;
            var domain = figure.GetParameterDomain();
            var oldParameter = pointOnFigure.Parameter;
            var figuresToRecalculate = GetFiguresToRecalculate();

            var step = (domain.Item2 - domain.Item1) / count;
            for (double lambda = domain.Item1; lambda < domain.Item2; lambda += step)
            {
                pointOnFigure.Parameter = lambda;
                figuresToRecalculate.ForEach(f => f.Recalculate());
                result.Add(point.Coordinates);
            }

            pointOnFigure.Parameter = domain.Item2;
            figuresToRecalculate.ForEach(f => f.Recalculate());
            result.Add(point.Coordinates);

            pointOnFigure.Parameter = oldParameter;
            figuresToRecalculate.ForEach(f => f.Recalculate());

            return result;
        }

        IEnumerable<IFigure> GetFiguresToRecalculate()
        {
            return DependencyAlgorithms.FindDescendants(f => f.Dependencies, new IFigure[] { this });
        }
    }
}
