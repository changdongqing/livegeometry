﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace DynamicGeometry
{
    public abstract class CoordinatesShapeBase<TShape> : ShapeBase<TShape>
        where TShape : UIElement
    {
        public override void MoveToCore(Point newLocation)
        {
            Coordinates = newLocation;
        }

        public override void UpdateVisual()
        {
            Shape.MoveTo(ToPhysical(Coordinates));
        }

        public Point Coordinates { get; set; }
    }
}
