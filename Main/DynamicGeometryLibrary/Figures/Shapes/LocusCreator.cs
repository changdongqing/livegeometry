﻿using System.Windows.Media;

namespace DynamicGeometry
{
    public class LocusCreator : FigureCreator
    {
        protected override IFigure CreateFigure()
        {
            return Factory.CreateLocus(Drawing, FoundDependencies);
        }

        protected override DependencyList InitExpectedDependencies()
        {
            return DependencyList.Create<IPoint, PointOnFigure>();
        }

        public override string Name
        {
            get { return "Locus"; }
        }

        public override string HintText
        {
            get
            {
                return "Click a point that depends on some point on figure.";
            }
        }

        public override System.Windows.Media.Color ToolButtonColor
        {
            get
            {
                return Color.FromArgb(255, 230, 215, 245);
            }
        }

        public override System.Windows.Controls.Panel CreateIcon()
        {
            return IconBuilder.BuildIcon()
                .Line(0, 0.7, 0.7, 0)
                .Line(0.3, 1, 1, 0.3)
                .Point(0.35, 0.35)
                .Canvas;
        }
    }
}
