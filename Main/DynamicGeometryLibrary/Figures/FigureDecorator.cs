﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;

namespace DynamicGeometry
{
    public abstract class FigureDecorator<T> : IFigure
        where T : IFigure
    {
        public string c_store_id { get; set; }
        public string c_guid { get; set; }

        public string c_type { get; set; }
        public bool c_is_modify { get; set; }
        public T Decorated { get; set; }

        public Drawing Drawing { get; set; }

        public IEnumerable<IFigure> Dependencies
        {
            get { return Decorated.Dependencies; }
            set { Decorated.Dependencies = value; }
        }

        public IFigureList Dependents
        {
            get { return Decorated.Dependents; }
        }

        public string Name
        {
            get
            {
                return Decorated.Name;
            }
            set
            {
                Decorated.Name = value;
            }
        }

        public bool Selected
        {
            get
            {
                return Decorated.Selected;
            }
            set
            {
                Decorated.Selected = value;
            }
        }
      
        public bool Exists
        {
            get
            {
                return Decorated.Exists;
            }
            set
            {
                Decorated.Exists = value;
            }
        }

        public virtual void Recalculate()
        {
            Decorated.Recalculate();
        }

        public virtual void UpdateVisual()
        {
            Decorated.UpdateVisual();
        }

        public virtual IFigure HitTest(Point point)
        {
            return Decorated.HitTest(point);
        }

        public virtual void OnAddingToCanvas(Canvas newContainer)
        {
            Decorated.OnAddingToCanvas(newContainer);
        }

        public virtual void OnRemovingFromCanvas(Canvas leavingContainer)
        {
            Decorated.OnRemovingFromCanvas(leavingContainer);
        }

        public int ZIndex
        {
            get
            {
                return Decorated.ZIndex;
            }
            set
            {
                Decorated.ZIndex = value;
            }
        }

        public bool Visible
        {
            get
            {
                return Decorated.Visible;
            }
            set
            {
                Decorated.Visible = value;
            }
        }

        public virtual void WriteXml(System.Xml.XmlWriter writer)
        {
            Decorated.WriteXml(writer);
        }

        public virtual void ReadXml(System.Xml.Linq.XElement element)
        {
            Decorated.ReadXml(element);
        }

        public void UpdateExistence()
        {
            Decorated.UpdateExistence();
        }

        public bool Equals(IFigure other)
        {
            return object.ReferenceEquals(this, other);
        }
    }
}
